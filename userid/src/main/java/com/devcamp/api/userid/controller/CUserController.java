package com.devcamp.api.userid.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.api.userid.model.CUser;
import com.devcamp.api.userid.repository.ICUserRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CUserController {

    @Autowired
    ICUserRepository pIcUserRepository;

    // GET ALL user
    @GetMapping("/users")
    public List<CUser> getAllUser(){
        return pIcUserRepository.findAll();
    }

    // GET ID user
    @GetMapping("/users/{userId}")
    public CUser getUserById(@PathVariable("userId") long userId){
        if (pIcUserRepository.findById(userId).isPresent()) {
            return pIcUserRepository.findById(userId).get();
        } else {
            return null;
        }
    }

    // POST user
    @PostMapping("/users")
    public ResponseEntity<Object> createUser(@RequestBody CUser cUser){
        
        try {
            CUser newUser = new CUser();
            newUser.setHoTen(cUser.getHoTen());
            newUser.setDiaChi(cUser.getDiaChi());
            newUser.setEmail(cUser.getEmail());
            newUser.setSoDienThoai(cUser.getSoDienThoai());
            newUser.setNgayTao(new Date());
            CUser saveUser = pIcUserRepository.save(newUser);
            return new ResponseEntity<>(saveUser,HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //PUT user
    @PutMapping("/users/{userId}")
    public ResponseEntity<Object> updateUser(@PathVariable("userId") Long userId, @RequestBody CUser cUser){
        Optional<CUser> userData = pIcUserRepository.findById(userId);
        if(userData.isPresent()){
            CUser newUser = userData.get();
            newUser.setHoTen(cUser.getHoTen());
            newUser.setDiaChi(cUser.getDiaChi());
            newUser.setEmail(cUser.getEmail());
            newUser.setSoDienThoai(cUser.getSoDienThoai());
            newUser.setNgayCapNhat(new Date());
            CUser savedUser = pIcUserRepository.save(newUser);
            try {
                return new ResponseEntity<>(savedUser,HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity().body("Failed to Update specified User: " +e.getCause().getCause().getMessage());
            }
        }else{
            return ResponseEntity.badRequest().body("Failed to get specified User: " + userId + " for update");
        }
    }

    @DeleteMapping("/users/{userId}")
    public ResponseEntity<Object> deleteUserById(@PathVariable Long userId){
        try {
            pIcUserRepository.deleteById(userId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin
	@GetMapping("/users/count")
	public long countUser() {
		return pIcUserRepository.count();
	}

    @CrossOrigin
	@GetMapping("/users/check/{userId}")
	public boolean checkUserById(@PathVariable Long userId) {
		return pIcUserRepository.existsById(userId);
	}
}
