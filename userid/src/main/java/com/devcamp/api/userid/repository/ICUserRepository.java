package com.devcamp.api.userid.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.api.userid.model.CUser;

public interface ICUserRepository extends JpaRepository<CUser,Long> {
    
}
